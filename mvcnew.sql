/*
 Navicat Premium Data Transfer

 Source Server         : DBB
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : mvc

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 31/10/2020 16:53:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT 1,
  `edit_status` tinyint(2) NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tasks
-- ----------------------------
INSERT INTO `tasks` VALUES (1, 'test', 'test1@test.test', 1, 0, 'test description');
INSERT INTO `tasks` VALUES (3, 'test3', 'test3@test.test', 0, 1, 'test3 edited');
INSERT INTO `tasks` VALUES (5, 'hdbhb', 'bchdbchbc@bhc.bdhbd', 0, 0, 'nduvh hgu hfug hfug hugvhurrhurhguhrhrugh hfurh urhrh urh rhurhu');
INSERT INTO `tasks` VALUES (6, 'dygyd', 'cndhcb@vcghvc.vchd', 0, 0, '');
INSERT INTO `tasks` VALUES (7, 'bhb', 'nfvjnvj@grg.g', 0, 0, 'bhbh');
INSERT INTO `tasks` VALUES (8, 'nn', 'bchdbchbc@bhc.bdhbd', 0, 0, 'nn');
INSERT INTO `tasks` VALUES (9, 'vyhuguiyg', 'bdh@vdgd.dhvv', 0, 0, 'ugugu');
INSERT INTO `tasks` VALUES (10, 'dhb', 'nn@vdfgdf.dvyd', 0, 0, 'hbhbh');
INSERT INTO `tasks` VALUES (11, 'Nbfchbh', 'ndfBH@vdg.dvhgddcbhbd', 0, 0, 'hbhbk');
INSERT INTO `tasks` VALUES (12, 'nffn', 'fbhfb2@vdg.gdufdfbh', 0, 0, 'ekrvuif');
INSERT INTO `tasks` VALUES (13, 'nffn', 'fbhfb2@vdg.gdufdfbh', 0, 0, 'ekrvuif');
INSERT INTO `tasks` VALUES (14, 'tesnenq', 'bghybg@vgddf.dgyd', 0, 0, 'dnfd');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '$2y$10$7JgDidVOMVw7XrQRxCDr7OiF6T7GAMKPDES2TnVGP5ylYZHUxRdIq');

SET FOREIGN_KEY_CHECKS = 1;
