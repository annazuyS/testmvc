<?php

session_start();
require '../app/init.php';
/**
 * Instance object App
 * @var app
 */
$app = new App;

/**
 * Call function run
 *
 */

$app->run();