<?php

class Task {

    private $db;
    private $table = 'tasks';

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAll($page, $sort)
    {
        $no_of_records_per_page = 3;
        $offset = ($page-1) * $no_of_records_per_page;

//        dd("SELECT * FROM {$this->table} " . $sort . " LIMIT $offset, $no_of_records_per_page");
        $this->db->query("SELECT * FROM {$this->table} " . $sort . " LIMIT $offset, $no_of_records_per_page");
        $tasks = $this->db->resultSet();
        $this->db->query("SELECT COUNT(*) FROM {$this->table}");
        $count = $this->db->single();
        if((int)$count['COUNT(*)'] != 0){
            $total = (int)$count['COUNT(*)'] / $no_of_records_per_page;
        }else {
            $total = 0;
        }
        return ['tasks' => $tasks,
            'total' => ceil($total)
        ];
    }

    public function store($name, $email, $description)
    {
        $this->db->query("INSERT INTO {$this->table} (name, email, description) VALUES (:name, :email, :description)");

        $this->db->bind(':name', $name);
        $this->db->bind(':email', $email);
        $this->db->bind(':description', $description);

        return $this->db->execute();
    }

    public function edit($id)
    {
        $this->db->query("SELECT * FROM {$this->table} WHERE id = {$id}");
        return $this->db->single();
    }

    public function update($id, $name, $email, $description, $status, $edit_status)
    {
        $this->db->query("UPDATE {$this->table} SET name = :name, email = :email, description = :description, status = :status, edit_status = :edit_status WHERE id = {$id}");

        $this->db->bind(':name', $name);
        $this->db->bind(':email', $email);
        $this->db->bind(':description', $description);
        $this->db->bind(':status', $status);
        $this->db->bind(':edit_status', $edit_status);

        return $this->db->execute();
    }

    public function destroy($id)
    {
        $this->db->query("DELETE FROM {$this->table} WHERE id = {$id}");
        return $this->db->execute();
    }

}