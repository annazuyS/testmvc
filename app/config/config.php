<?php

/**
 * BASEURL
 */

define('BASEURL', 'http://localhost/crud');
//define('BASEURL', 'http://127.0.0.1:8080/mvc/public');

/**
 * DATABASE Connection
 */

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'mvc');