<div class="container">
    <div class="jumbotron mt-4">
        <h1 class="display-4"><?= $data['title']; ?></h1>

        <div class="jumbotron">
            <?php
            $text = "";
            if(isset($data['message'])){
                if(isset($data['error'])){
                    $text .= "<h2 style='color: red'>" . $data['message'] ."</h2>";
                }else{
                    $text .= "<h2 style='color: green'>" . $data['message'] ."</h2>";

                }
            }
            ?>
            <?= $text ?>
            <form action="<?= BASEURL ?>/task/store" method="post">
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                </div>
                <div class="form-group">
                    <label>name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter name" required>
                </div>
                <div class="form-group">
                    <label>description</label>
                    <input type="text" class="form-control" name="description" placeholder="Enter description">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

        <ul class="pagination">
            <?php
            $urlStringPage = "";
            $urlStringSort = "";
            if(isset($data['sort'])){
                $urlStringSort = "&sort=" . $data['sort'];
            }

            if($data['page'] != 1){
                $urlStringPage = "&page=" . $data['page'];
            }

            ?>
            <li><a href="?page=1<?= $urlStringSort;?>">1</a></li>

            <?php
            for($i = 2; $i < $data['total_pages']; $i ++){
            ?>
                <li>
                    <a href="<?php if($data['page'] == $i){ echo '#'; } else { echo "?page=".$i . $urlStringSort; } ?>"><?= $i; ?></a>
                </li>
            <?php }?>

            <li><a href="?page=<?php echo $data['total_pages'] . $urlStringSort; ?>"><?= $data['total_pages']; ?></a></li>
        </ul>

        <div class="table-responsive mt-5">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name
                        <a href="?sort=name_asc<?= $urlStringPage; ?>"><i class="fa fa-fw fa-sort-up"></i></a>
                        <a href="?sort=name_desc<?= $urlStringPage; ?>"><i class="fa fa-fw fa-sort-down"></i></a>
                    <th scope="col">Email
                        <a href="?sort=email_asc<?= $urlStringPage; ?>"><i class="fa fa-fw fa-sort-up"></i></a>
                        <a href="?sort=email_desc<?= $urlStringPage; ?>"><i class="fa fa-fw fa-sort-down"></i></a></th>
                    <th scope="col">Status
                        <a href="?sort=status_asc<?= $urlStringPage; ?>"><i class="fa fa-fw fa-sort-up"></i></a>
                        <a href="?sort=status_desc<?= $urlStringPage; ?>"><i class="fa fa-fw fa-sort-down"></i></a></th>
                    <th scope="col">Edited Status</th>
                    <th scope="col">Description</th>
                    <?php if(isset($_SESSION['user_id'])){ ?>
                    <th scope="col">Action</th>
                    <?php }?>
                </tr>
                </thead>
                <tbody>
                <?php
                $statuses = ["not done", "done"];
                $statuses_edited = ["not edited", "edited"];
                foreach ($data['task'] as $key => $value):?>
                    <tr>
                        <th scope="row"><?= $value['id']; ?></th>
                        <td><?= $value['name']; ?></td>
                        <td><?= $value['email']; ?></td>
                        <td><?= $statuses[$value['status']]; ?></td>
                        <td><?= $statuses_edited[$value['edit_status']]; ?></td>
                        <td><?= substr($value['description'], 0, 20); ?></td>
                        <?php if(isset($_SESSION['user_id'])){ ?>
                        <td>
                            <a href="<?= BASEURL; ?>/task/edit/<?= $value['id']; ?>" class="btn btn-xs btn-info">Edit</a>
                            <form action="<?= BASEURL; ?>/task/destroy/<?= $value['id'] ?>" method="post">
                                <button class="btn btn-xs btn-danger">Delete</button>
                            </form>
                        </td>
                        <?php } ?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>