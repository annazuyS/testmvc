<div class="container">
    <div class="jumbotron">
        <form action="<?= BASEURL ?>/task/update/<?= $data['task']['id'] ?>" method="post">
            <div class="form-group">
                <label>Email address</label>
                <input type="text" class="form-control" name="email" placeholder="Enter email" value="<?= $data['task']['email']; ?>">
            </div>
            <div class="form-group">
                <label>name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter name" value="<?= $data['task']['name']; ?>">
            </div>
            <div class="form-group">
                <label>Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="0" <?php if($data['task']['status'] == 0){?> selected<?php }?>>not done</option>
                    <option value="1" <?php if($data['task']['status'] == 1){?> selected<?php }?>>done</option>
                </select>
            </div>
            <div class="form-group">
                <label>description</label>
                <input type="text" class="form-control" name="description" placeholder="Enter description" value="<?= $data['task']['description']; ?>">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>