<div class="container">
    <div class="jumbotron">
        <form action="<?= BASEURL ?>/login/data" method="post">
            <div class="form-group">
                <label>Login</label>
                <input type="text" class="form-control" name="login" placeholder="Enter login">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" placeholder="Enter password">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>