<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $data['title'] ?></title>
	<link rel="stylesheet" href="<?= BASEURL; ?>/assets/css/bootstrap.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
      <?php if(isset($_SESSION['user_id'])){ ?>
          <a class="navbar-brand" href="<?= BASEURL;  ?>/login/logout">logout</a>
      <?php  }else {?>
          <a class="navbar-brand" href="<?= BASEURL;  ?>/login">login</a>
      <?php }?>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="<?= BASEURL;  ?>">Home <span class="sr-only">(current)</span></a>
	      </li>
	    </ul>
	  </div>
  </div>
</nav>