<?php

class taskController extends Controller {

    public function index()
    {
        $data['title'] = 'Home';

        if (isset($_GET['page'])) {
            $data['page'] = $_GET['page'];
        } else {
            $data['page'] = 1;
        }

        $sort = "";
        if (isset($_GET['sort'])) {
            $data['sort'] = $_GET['sort'];
            $sort_string = $_GET['sort'];
            $sort_array = explode('_', $sort_string);
            $sort = "ORDER BY " . $sort_array[0] . " " . $sort_array[1];
        }
        $result = $this->model('Task')->getAll($data['page'], $sort);

        $data['task'] = $result['tasks'];
        $data['total_pages'] = $result['total'];


        if(isset($_SESSION['error'])){
            $data['error'] = true;
            unset($_SESSION['error']);
        }

        if(isset($_SESSION['message'])){
            $data['message'] = $_SESSION['message'];
            unset($_SESSION['message']);
        }


        $this->view('templates/header', $data);
        $this->view('task/index', $data);
        $this->view('templates/footer');
    }

    public function store()
    {
        $name  = $_POST['name'];
        $email = $_POST['email'];
        $description = $_POST['description'];
        $message = "";
        if($_POST['email'] == ''){
            $message .= "Email field is required";
        }elseif($_POST['name'] == ''){
            $message .= "Name field is required";
        }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $message = "Invalid email format";
        }


        if ($message != ""){
            $_SESSION['error'] = true;
            $_SESSION['message'] = $message;
        }else{
            $this->model('Task')->store($name, $email,$description);
            $_SESSION['message'] = "Task created successfully";
        }

        $this->redirect('task');

    }

    public function edit($id)
    {
        if(isset($_SESSION['user_id'])){
            $data['title'] = 'Edit Data Task';

            $data['task'] = $this->model('Task')->edit($id);

            $this->view('templates/header', $data);
            $this->view('task/edit', $data);
        }else{
            $this->redirect('task');
        }

    }

    public function update($id)
    {
        $name  = $_POST['name'];
        $email = $_POST['email'];
        $description = $_POST['description'];
        $status = $_POST['status'];
        $edit_status = 0;


        $task = $this->model('Task')->edit($id);
        if($task['description'] != $_POST['description']){
            $edit_status = 1;
        }

        $this->model('Task')->update($id, $name, $email, $description, $status, $edit_status);

        $this->redirect('task');
    }

    public function destroy($id)
    {
        if(isset($_SESSION['user_id'])) {
            $this->model('Task')->destroy($id);
        }
        $this->redirect('task');
    }



}