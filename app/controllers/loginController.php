<?php

class loginController extends Controller
{

    public function index()
    {
        $data['title'] = "Login";
        $this->view('templates/header', $data);
        $this->view('auth/login');
        $this->view('templates/footer');
    }

    public function data()
    {
        $message = "";
        if($_POST['login'] == ''){
            $message .= "Login field is required";
        }elseif($_POST['password'] == ''){
            $message .= "Password field is required";
        }else{
            $user = $this->model('User')->find($_POST['login']);

            if(!$user){
                $message = "the entered data is not correct";
            }else{
                if(password_verify($_POST['password'], $user['password'])){

                    $_SESSION['user_id'] = $user['id'];
                }else {
                    $message = "the entered data is not correct";

                }

            }
        }

        if($message != ""){
            $_SESSION['error'] = true;
            $_SESSION['message'] = $message;
        }
        $this->redirect('task');
    }

    public function logout()
    {
        unset($_SESSION['user_id']);
        $this->redirect('task');

    }
}